# Invert binary tree

Inverting node children seems to me as model behavior, so I put `flip` method into `BinaryNode` class.

Next, there is `Inflector` class with method `invertTree` which uses recursion to invert all the tree.

# How to run

To execute tests, run:

```
composer install
./vendor/bin/phpunit --bootstrap ./vendor/autoload.php tests
```
