<?php

namespace App;

class Inflector
{
    /**
     * Invert's tree (flips left and right children).
     *
     * @param BinaryNode $node
     * @return BinaryNode
     */
    public static function invertTree(BinaryNode $node)
    {
        if ($node->hasChildren()) {
            $node->invertChildren();
        }
        if ($node->left instanceof BinaryNode) {
            static::invertTree($node->left);
        }
        if ($node->right instanceof BinaryNode) {
            static::invertTree($node->right);
        }
        return $node;
    }
}
