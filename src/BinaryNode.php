<?php

namespace App;

class BinaryNode
{
    /**
     * @var mixed node value
     */
    public $value = null;

    /**
     * @var BinaryNode|null
     */
    public $left = null;

    /**
     * @var BinaryNode|null
     */
    public $right = null;

    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * Reports whether node has at least one children.
     *
     * @return bool
     */
    public function hasChildren()
    {
        return $this->left !== null || $this->right !== null;
    }

    /**
     * Flips node's children.
     *
     * @return void
     */
    public function invertChildren()
    {
        $tmp = $this->left;
        $this->left = $this->right;
        $this->right = $tmp;
    }
}
